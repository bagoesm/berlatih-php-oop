<?php

class Animal
{
    public $legs = 2;
    public $cold_blooded = false;
    public $names;

    function __construct($names){
        $this->names = $names;
    }

    function get_names(){
        return $this->names;
    }
    function get_legs(){
        return $this->legs;
    }
    function get_cold_blooded(){
        return $this->cold_blooded;
    }

}

?>